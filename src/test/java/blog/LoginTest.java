package blog;

import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class LoginTest {
    private final String baseUrl = "https://test-stand.gb.ru/gateway/login";

    @Test
    public void validDataTest() {
        given()
                .when()
                .formParam("username","Mary Mary")
                .formParam("password","d9aed81b69")
                .post(baseUrl)
                .then()
                .statusCode(200);
    }

    @Test
    public void invalidDataTest() {
        given()
                .when()
                .formParam("username","Mary")
                .formParam("password","inv")
                .post(baseUrl)
                .then()
                .statusCode(401);
    }

    @Test
    public void emptyDataTest() {
        given()
                .when()
                .formParam("username","")
                .formParam("password","")
                .post(baseUrl)
                .then()
                .statusCode(401);
    }
}
