package blog;


import io.restassured.http.Header;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class MyPostsTest extends AbstractPostsTest {

    public MyPostsTest() {
        super("https://test-stand.gb.ru/api/posts");
    }

    @Test
    public void myNoAuthTest() {
        notAuthTest();
    }

    @Test
    public void mySimpleSuccessTest() {
        simpleSuccessTest();
    }

    @Test
    public void myResponseHasDataKeyTest() {
        responseHasDataKeyTest();
    }

    @Test
    public void mySuccessWithParamsTest() {
        successWithParamsTest("id", "ASC", "1");
        successWithParamsTest("createdAt", "DESC", "3");
        successWithParamsTest("title", "ALL", "2");
    }

    @Test
    public void myResponseHasMetaKeyTest() {
        responseHasMetaKeyTest();
    }

    @Test
    protected void myNegativePageTest() {
        negativePageTest();
    }

}
