package blog;


import org.junit.jupiter.api.Test;


public class NotMyPostsTest extends AbstractPostsTest {

    public NotMyPostsTest() {
        super("https://test-stand.gb.ru/api/posts?owner=notMe");
    }

    @Test
    public void notMyNoAuthTest() {
        notAuthTest();
    }

    @Test
    public void notMySimpleSuccessTest() {
        simpleSuccessTest();
    }

    @Test
    public void notMyResponseHasDataKeyTest() {
        responseHasDataKeyTest();
    }

    @Test
    public void notMySuccessWithParamsTest() {
        successWithParamsTest("id", "ASC", "1");
        successWithParamsTest("createdAt", "DESC", "3");
        successWithParamsTest("title", "ALL", "2");
    }

    @Test
    public void notMyResponseHasMetaKeyTest() {
        responseHasMetaKeyTest();
    }

    @Test
    protected void notMyNegativePageTest() {
        negativePageTest();
    }
}
