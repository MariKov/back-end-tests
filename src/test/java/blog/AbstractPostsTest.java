package blog;

import io.restassured.http.Header;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public abstract class AbstractPostsTest {
    private final String authKey = "01e2cf9d9fac375f992ec3fd158c03fb";
    private final String baseUrl;

    public AbstractPostsTest(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    //кейс 1
    protected void notAuthTest() {
        given()
                .when()
                .get(baseUrl)
                .then()
                .statusCode(401);
    }

    //кейс 2
    protected void simpleSuccessTest() {
        given()
                .when()
                .header(new Header("X-Auth-Token", authKey))
                .get(baseUrl)
                .then()
                .statusCode(200);
    }

    //кейс 3
    protected void responseHasDataKeyTest() {
        JsonPath response = given()
                .when()
                .header(new Header("X-Auth-Token", authKey))
                .get(baseUrl)
                .body()
                .jsonPath();
        assertNotNull(response.get("data"));
    }

    //кейс 4
    protected void successWithParamsTest(String sort, String order, String page) {
        given()
                .when()
                .header(new Header("X-Auth-Token", authKey))
                .queryParam("sort", sort)
                .queryParam("order", order)
                .queryParam("page", page)
                .get(baseUrl)
                .then()
                .statusCode(200);
    }

    //кейс 5
    protected void responseHasMetaKeyTest() {
        JsonPath response = given()
                .when()
                .header(new Header("X-Auth-Token", authKey))
                .queryParam("page", "2")
                .get(baseUrl)
                .body()
                .jsonPath();

        assertNotNull(response.get("meta"));
        assertNotNull(response.get("meta.count"));
        assertNotNull(response.get("meta.prevPage"));
    }

    //кейс 6
    protected void negativePageTest() {
        given()
                .when()
                .header(new Header("X-Auth-Token", authKey))
                .queryParam("page", "-1")
                .get(baseUrl)
                .then()
                .statusCode(400);

    }
}
